import vue from '@vitejs/plugin-vue';
import { minify } from 'html-minifier';
import { Plugin } from 'vite';
import viteImagemin from 'vite-plugin-imagemin';

const isMaster = process.env.NODE_ENV === 'production';

const vuePlugin: Plugin = vue({
	template: {
		compilerOptions: {
			isCustomElement: (tag: string) => {
				return tag === 'app';
			},
		},
	},
});



const imageMinPlugin: Plugin = (
	isMaster ?
		viteImagemin({
			filter: /\.svg$/i,
			gifsicle: false,
			webp: false,
			mozjpeg: false,
			pngquant: false,
			optipng: false,
			svgo: {
				plugins: [
					{
						removeViewBox: false,
					},
					{
						cleanupIDs: false,
					},
				],
			},
		}) :
		undefined
);


const minifyHtmlPlugin: Plugin = {
	name: 'minifyHtml',
	apply: 'build',
	enforce: 'post',
	transformIndexHtml: (html) => {
		if (!isMaster) {
			return html;
		}

		return minify(html, {
			collapseWhitespace: true,
			minifyCSS: true,
			minifyJS: true,
			removeAttributeQuotes: true,
			removeComments: true,
		});
	},
};


export default [
	vuePlugin,
	minifyHtmlPlugin,
	imageMinPlugin,
];
