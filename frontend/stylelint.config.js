module.exports = {
	extends: 'stylelint-config-standard',
	ignoreFiles: [
		'*.svg',
		'*.xml',
	],
	plugins: [
		'stylelint-scss',
		'stylelint-order',
	],
	rules: {
		'at-rule-no-unknown': [
			true,
			{
				ignoreAtRules: [
					'extend',
					'include',
					'mixin',
					'if',
					'for',
					'each',
					'while',
				],
			},
		],
		'declaration-block-no-duplicate-properties': null,
		'font-family-no-missing-generic-family-keyword': null,
		indentation: 'tab',
		'no-descending-specificity': null,
		'no-empty-first-line': true,
		'order/order': [
			'custom-properties',
			'dollar-variables',
			'declarations',
			'at-rules',
			'rules',
		],
		'scss/at-import-partial-extension': 'never',
		'scss/dollar-variable-colon-space-after': 'always',
		'scss/dollar-variable-colon-space-before': 'never',
		'selector-pseudo-element-no-unknown': [
			true,
			{
				ignorePseudoElements: [
					'v-deep',
				],
			},
		],
		'selector-type-no-unknown': null,
		'unit-no-unknown': null,
	},
};
