export interface PCB {
	id: number;
	pcb_id: number;
	quantity: number;
	startDate: string;
	endDate: string;
}
