import { Vue } from 'vue-class-component';

export default class DummyComponent extends Vue {
	created() {
		//
	}

	mounted() {
		console.log('Dummy component works!');
	}
}
