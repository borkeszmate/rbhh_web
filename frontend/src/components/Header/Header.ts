import { Vue, Options } from 'vue-class-component';
import { navigateTo } from '/@/helpers/scrollto';

@Options({
	watch: {
		$route() {
			this.opened = false;
			this.mobileNavigationOpen = false;
			document.querySelector('body')?.classList.remove('overflow-hidden');
		},
	},
	components: {},
})
export default class Header extends Vue {
	mobileView: boolean = false;
	mobileNavigationOpen: boolean = false;
	navigateTo = navigateTo;

	created() {
		this.handleView();
		window.addEventListener('resize', this.handleView);
	}


	handleView() {
		this.mobileView = window.innerWidth <= 990;
	}

	toggleMobileNavigation() {
		this.mobileNavigationOpen = !this.mobileNavigationOpen;

		if (this.mobileNavigationOpen) {
			document.querySelector('body')?.classList.add('overflow-hidden');
		} else {
			document.querySelector('body')?.classList.remove('overflow-hidden');
		}
	}
}
