import { Vue } from 'vue-class-component';

export default class CookieBanner extends Vue {
	visible: boolean = false;

	mounted() {
		this.visible = window.localStorage.getItem('cookie_policy') !== 'v1' &&
					window.navigator.userAgent.indexOf('prerender') === -1;
	}

	accept() {
		this.visible = false;
		window.localStorage.setItem('cookie_policy', 'v1');
	}
}
