import router from '/@/router';

export const navigateTo = (id: string, correctionDesktop: number = 0, correctionMobile: number = 0 ) => {
	router.replace({
		path: id,
		force: true,
	});

	setTimeout(() => {
		const top = (document.querySelector('#' + id) as HTMLElement).getBoundingClientRect().top;
		window.scrollTo({
			top: (window.scrollY + top) + (window.innerWidth >= 992 ? correctionDesktop : correctionMobile),
			left: 0,
			behavior: 'auto',
		});
	}, 10);
};
