export const styleAsCurrency = (amount: number, currency: string = 'Ft'): string => {
	const num: string = new Intl.NumberFormat('de', {
		style: 'decimal',
	}).format(amount);

	return num + ' ' + currency;
};

export const getDifferenceInPercentage = (a: number, b: number ): string => {
	return Math.round(((a - b) / b) * 100) + '%';
};


export const numberWithSpaces = (x: number | string) => {
	return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
};

export const replaceDashesToDots = (input: string ): string => {
	return input.replace(new RegExp('-', 'gm'), '.');
};
