import gsap from 'gsap';

const onEnter = (el: any) => {
	gsap.to(el, {
		opacity: 1,
		height: '1.6em',
		delay: el.dataset.index * 0.05,
	});
};

const onBeforeEnter = (el: any) => {
	el.style.opacity = 0;
	el.style.height = 0;
};

const onLeave = (el: any) => {
	gsap.to(el, {
		opacity: 0,
		height: 0,
		delay: el.dataset.index * 0.2,
	});
};

export {
	onEnter,
	onBeforeEnter,
	onLeave,
};
