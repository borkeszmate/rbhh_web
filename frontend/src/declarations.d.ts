declare module '*.vue' {
	import type { defineComponent } from 'vue';

	const component: ReturnType<typeof defineComponent>;
	export default component;
}

declare module '*.svg' {
	import type { defineComponent } from 'vue';

	const component: ReturnType<typeof defineComponent>;
	export default component;
}

declare module '*.jpg' {
	import type { defineComponent } from 'vue';

	const component: ReturnType<typeof defineComponent>;
	export default component;
}

declare module '*.png' {
	import type { defineComponent } from 'vue';

	const component: ReturnType<typeof defineComponent>;
	export default component;
}

declare module '*.webp' {
	import type { defineComponent } from 'vue';

	const component: ReturnType<typeof defineComponent>;
	export default component;
}

/// <reference types="vite/client" />

declare module '*.vue' {
	import type { DefineComponent } from 'vue'
	// eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
	const component: DefineComponent<{}, {}, any>
	export default component
  }




declare module 'vue-svg-inline-plugin';
declare module 'lazysizes';
declare module 'plyr';
declare module 'plyr/dist/plyr.polyfilled';
declare module 'vue-select';
declare module 'jquery';
