import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
	{
		path: '/',
		component: () => import('/@/pages/Home/Home.vue'),
	},
	{
		path: '/center',
		component: () => import('/@/pages/Center/Center.vue'),
	},
	{
		path: '/about',
		component: () => import('/@/pages/About/About.vue'),
	},
	{
		path: '/404',
		component: () => import('/@/layouts/NotFound/NotFound.vue'),
	},
	{
		path: '/:pathMatch(.*)*',
		alias: '/404',
		component: () => import('/@/layouts/NotFound/NotFound.vue'),
	},
];

export default routes;
