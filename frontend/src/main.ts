import { createApp } from 'vue';
import VueSvgInlinePlugin from 'vue-svg-inline-plugin';

import App from '/@/layouts/App/App.vue';
import vSelect from 'vue-select';
import router from '/@/router';


/////////
// Vue //
/////////
const app = createApp(App);

app
	.use(router)
	.use(VueSvgInlinePlugin)
	.component('v-select', vSelect);


app.mount('#vue');


document.body.classList.remove('loading');
