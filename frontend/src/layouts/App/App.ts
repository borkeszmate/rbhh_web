import { Options, Vue } from 'vue-class-component';
import Footer from '/@/components/Footer/Footer.vue';
import Header from '/@/components/Header/Header.vue';

import 'lazysizes';


@Options({
	components: {
		Footer,
		Header,
	},
})
class App extends Vue {
	footerVisible: boolean = false;
	windowHeight?: number = undefined;

	created() {
		this.$router.isReady().then(() => {
			this.setVh();

			setTimeout(() => {
				this.footerVisible = true;
				this.setVh();
			}, 1000);
		});
	}

	mounted() {
		this.setDarkFavicon();

		this.setVh();
		window.addEventListener('resize', this.setVh);
	}

	setDarkFavicon() {
		if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
			const favicon = document.querySelector('link[rel="icon"]');

			if (favicon) {
				favicon.setAttribute('href', '/favicon-white.png');
			}
		}
	}

	setVh() {
		if (this.windowHeight === window.innerHeight) {
			return;
		} else {
			this.windowHeight = window.innerHeight;
		}

		let windowHeight = this.windowHeight;

		if (windowHeight % 10 === 9) {
			windowHeight++;
		}

		const vh = windowHeight * 0.01;
		document.documentElement.style.setProperty('--vh', vh + 'px');
	}
}

export default App;
