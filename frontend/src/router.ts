import { createRouter, createWebHistory, Router } from 'vue-router';
import routes from './routes';

const router: Router = createRouter({
	history: createWebHistory(),
	routes,
	scrollBehavior: (to: any, from: any, savedPosition: any) => {
		if (to.hash) {
			return {
				el: to.hash,
				behavior: 'smooth',
			};
		}

		if (to.path === from.path && to.query !== from.query) {
			return false;
		}

		if (to.name && from.name && to.name === from.name) {
			return false;
		}

		if (savedPosition) {
			return savedPosition;
		}

		return {
			top: 0,
			behavior: 'smooth',
		};
	},
});

export default router;
