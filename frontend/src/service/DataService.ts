import ProductionsRequests from './requests/ProductionsRequests';
import RandomRequests from './requests/RandomRequests';

export default {
	production: ProductionsRequests,
	random: RandomRequests,
};
