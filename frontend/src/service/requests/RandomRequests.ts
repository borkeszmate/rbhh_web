import Api from '../Api';


const RandomRequests = {
	getRandomWord: () => {
		return Api.get('https://random-word-api.herokuapp.com/word');
	},
};

export default RandomRequests;
