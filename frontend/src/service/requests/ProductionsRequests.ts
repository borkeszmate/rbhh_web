import Api from '../Api';


const ProductionsRequests = {
	getProductions: () => {
		return Api.get('/productions');
	},
	deleteProduction: (payload: any) => {
		return Api.post('/productions/delete', payload);
	},
};

export default ProductionsRequests;
