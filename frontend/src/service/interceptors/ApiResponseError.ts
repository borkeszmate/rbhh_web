const ApiResponseError = (error: any) => {
	return Promise.reject(error);
};

export default ApiResponseError;
