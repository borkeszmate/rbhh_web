export default {
	moreDetails: 'More details',
	ok: 'Ok',
	backToHome: 'Back to homepage',
	search: 'Search...',
	filter: 'Filters',
	back: 'Back',
	cancel: 'Cancel',
	submit: 'Submit',
	select: 'Select',
	next: 'Next',
};
