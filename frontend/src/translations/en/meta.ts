export default {
	title: '#LikeABosch',
	ogTitle: '#LikeABosch',

	description: '#LikeABosch',
	ogDescription: '#LikeABosch',

	ogImage: window.location.origin + '/share.jpg',
};
