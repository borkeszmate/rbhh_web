import cookie from './components/cookie';
import footer from './components/footer';
import header from './components/header';
import errors from './errors';
import general from './general';
import meta from './meta';
import navigation from './navigation';
import contact from './pages/contact';
import home from './pages/home';
import notFound from './pages/notFound';
import routes from './routes';

export default {
	routes,
	navigation,
	general,
	meta,
	errors,
	components: {
		header,
		footer,
		cookie,
	},
	pages: {
		notFound,
		home,
		contact,
	},
};
