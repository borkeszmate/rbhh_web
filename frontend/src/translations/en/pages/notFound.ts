export default {
	title: 'Page not found',
	subtitle: 'You might have dispelled the URL or the artwork you were looking for is no longer available. Whatever happened, click on the link below and keep looking on the site!',
};
