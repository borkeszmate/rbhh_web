export default {
	contact: 'Contact',
	form: {
		fullName: 'Full name*',
		fullNamePlaceholder: 'Please type your full name here',
		email: 'Email address*',
		emailPlaceholder: 'Please type your email address here',
		message: 'Message',
		messagePlaceholder: 'Please write your message here',
		consent: '<span>I agree and consent to the <a href="/pdf/adatkezelesi-tajekoztato.pdf" target="_blank">Privacy Policy</a>.</span>',
	},
	thankYou: {
		title: 'Thank you for your message!',
		description: 'We will contact you shortly.',
	},
};
