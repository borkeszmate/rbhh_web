import en from '/@/translations/en';
import hu from '/@/translations/hu';

export default {
	hu,
	en,
};
