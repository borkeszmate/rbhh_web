export default {
	copy: 'Weboldalunkon cookie-kat használunk annak érdekében, hogy jobb felhasználói élményt biztosítsuk számodra. <a href="/pdf/cookie-tajekoztato.pdf" target="_blank">Cookie tájékoztató</a>',
	button: 'Elfogadom',
};
