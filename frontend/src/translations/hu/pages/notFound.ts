export default {
	title: 'A keresett oldal nem található',
	subtitle: 'Lehet, hogy elgépelted az URL-t, vagy az alkotás, amit kiszemeltél, már nem elérhető. Bármi is történt, kattints az alábbi linkre, és nézelődj tovább nálunk!',
};
