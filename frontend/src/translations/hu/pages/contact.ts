export default {
	contact: 'Kapcsolat',

	form: {
		fullName: 'Teljes név*',
		fullNamePlaceholder: 'Kérjük, add meg a teljes neved',
		email: 'Email*',
		emailPlaceholder: 'Kérjük, add meg az email címed',
		message: 'Üzenet',
		messagePlaceholder: 'Kérjük, írd ide az üzeneted',
		consent: '<span>Elfogadom az <a href="/pdf/adatkezelesi-tajekoztato.pdf" target="_blank">Adatkezelési tájékoztatót</a>.</span>',
	},
	thankYou: {
		title: 'Köszönjük üzeneted!',
		description: 'Kollégánk hamarosan felveszi veled a kapcsolatot.',
	},
};
