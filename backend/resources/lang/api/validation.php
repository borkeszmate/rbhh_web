<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	'accepted' => ':attribute:required',
	'active_url' => ':attribute:invalid',
	'after' => ':attribute:too_early',
	'after_or_equal' => ':attribute:too_early',
	'alpha' => ':attribute:invalid',
	'alpha_dash' => ':attribute:invalid',
	'alpha_num' => ':attribute:invalid',
	'array' => ':attribute:invalid',
	'before' => ':attribute:too_late',
	'before_or_equal' => ':attribute:too_late',
	'between' => [
		'numeric' => ':attribute:invalid',
		'file' => ':attribute:invalid',
		'string' => ':attribute:invalid',
		'array' => ':attribute:invalid',
	],
	'boolean' => ':attribute:invalid',
	'confirmed' => ':attribute:invalid',
	'date' => ':attribute:invalid',
	'date_equals' => ':attribute:invalid',
	'date_format' => ':attribute:invalid',
	'different' => ':attribute:invalid',
	'digits' => ':attribute:invalid',
	'digits_between' => ':attribute:invalid',
	'dimensions' => ':attribute:invalid',
	'distinct' => ':attribute:invalid',
	'email' => ':attribute:invalid',
	'ends_with' => ':attribute:invalid',
	'exists' => ':attribute:invalid',
	'file' => ':attribute:invalid',
	'filled' => ':attribute:required',
	'gt' => [
		'numeric' => ':attribute:invalid',
		'file' => ':attribute:invalid',
		'string' => ':attribute:invalid',
		'array' => ':attribute:invalid',
	],
	'gte' => [
		'numeric' => ':attribute:invalid',
		'file' => ':attribute:invalid',
		'string' => ':attribute:invalid',
		'array' => ':attribute:invalid',
	],
	'image' => ':attribute:invalid',
	'in' => ':attribute:invalid',
	'in_array' => ':attribute:invalid',
	'integer' => ':attribute:invalid',
	'ip' => ':attribute:invalid',
	'ipv4' => ':attribute:invalid',
	'ipv6' => ':attribute:invalid',
	'json' => ':attribute:invalid',
	'lt' => [
		'numeric' => ':attribute:invalid',
		'file' => ':attribute:invalid',
		'string' => ':attribute:invalid',
		'array' => ':attribute:invalid',
	],
	'lte' => [
		'numeric' => ':attribute:invalid',
		'file' => ':attribute:invalid',
		'string' => ':attribute:invalid',
		'array' => ':attribute:invalid',
	],
	'max' => [
		'numeric' => ':attribute:invalid',
		'file' => ':attribute:invalid',
		'string' => ':attribute:invalid',
		'array' => ':attribute:invalid',
	],
	'mimes' => ':attribute:invalid',
	'mimetypes' => ':attribute:invalid',
	'min' => [
		'numeric' => ':attribute:invalid',
		'file' => ':attribute:invalid',
		'string' => ':attribute:invalid',
		'array' => ':attribute:invalid',
	],
	'not_in' => ':attribute:invalid',
	'not_regex' => ':attribute:invalid',
	'numeric' => ':attribute:invalid',
	'password' => ':attribute:invalid',
	'present' => ':attribute:required',
	'regex' => ':attribute:invalid',
	'required' => ':attribute:required',
	'required_if' => ':attribute:required',
	'required_unless' => ':attribute:required',
	'required_with' => ':attribute:required',
	'required_with_all' => ':attribute:required',
	'required_without' => ':attribute:required',
	'required_without_all' => ':attribute:required',
	'same' => ':attribute:invalid',
	'size' => [
		'numeric' => ':attribute:invalid',
		'file' => ':attribute:invalid',
		'string' => ':attribute:invalid',
		'array' => ':attribute:invalid',
	],
	'starts_with' => ':attribute:invalid',
	'string' => ':attribute:invalid',
	'timezone' => ':attribute:invalid',
	'unique' => ':attribute:invalid',
	'uploaded' => ':attribute:invalid',
	'url' => ':attribute:invalid',
	'uuid' => ':attribute:invalid',

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'attribute-name' => [
			'rule-name' => ':attribute:invalid',
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap our attribute placeholder
	| with something more reader friendly such as "E-Mail Address" instead
	| of "email". This simply helps us make our message more expressive.
	|
	*/

	'attributes' => [],

];
