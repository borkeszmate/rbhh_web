<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PurchaseInnerEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $purchase;

    public function __construct($purchase)
    {
        $this->purchase = $purchase;
    }

    public function build()
    {
        $address = env('MAIL_FROM_ADDRESS', 'MAIL_FROM_NAME');
        $subject = 'Új megrendelés történt';
        $name = 'Real Estate Beach Conference';

        return $this->view('emails/purchase-inner/purchase-inner')
                    ->from($address, $name)
                    ->replyTo($address, $name)
                    ->subject($subject)
                    ->attach(\storage_path() . '/app/public/pdf/dijbekero_'. $this->purchase->id . '.pdf');
    }
}
