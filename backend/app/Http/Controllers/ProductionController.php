<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Response;


use App\Models\Production;

use App\Http\Requests\ProductionDeleteRequest;

class ProductionController extends BaseController
{
    public $productions;

    public function getProductions():JsonResponse
    {
        $productions = Production::all();
        return response()->json($productions);
    }

    public function deleteProduction(ProductionDeleteRequest $request): JsonResponse
    {
        $id = $request->id;

        $item = Production::find($id);

        if ($item) {
            $item->forceDelete();

            return response()->json([
                'id' => $id
            ]);
        }


        return Response::json([
            'code'      =>  404,
            'message'   =>  'not_found'
        ], 404);
    }
}
