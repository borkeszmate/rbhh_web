<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

class Cors
{
    /**
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->getMethod() === 'OPTIONS') {
            $response = response('');
        } else {
            $response = $next($request);
        }

        if (
            !($response instanceof Response) &&
            !($response instanceof JsonResponse)
        ) {
            return $response;
        }

        return $response
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
            ->header('Access-Control-Allow-Headers', implode(', ', $this->getAllowHeaders()))
            ->header('Access-Control-Expose-Headers', implode(', ', $this->getExposeHeaders()));
    }



    protected function getAllowHeaders(): array
    {
        return [
            'Content-Type',
            'Authorization',
        ];
    }



    protected function getExposeHeaders(): array
    {
        return [
            'Cache-Control',
            'Content-Language',
            'Content-Length',
            'Content-Type',
            'Expires',
            'Last-Modified',
            'Pragma',
            'Authorization',
        ];
    }
}
