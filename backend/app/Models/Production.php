<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    public $table = 'production';
    protected $guarded = ['id'];
}
